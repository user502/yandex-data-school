#include <iostream>
#include <fstream>
#include <string>
 
using std::fstream;
using std::cin;
using std::cout;
 
int main() {
  int current, previous, index;
  fstream input("input.txt", fstream::in);
  fstream output("output.txt", fstream::out);
  input >> index; //��� cin >> index;
  previous = 0;
  current = 1;
  for (int i = 0; i < index; ++i) {
    int next = (previous + current) % 10;
    previous = current % 10;
    current = next;
  }
  output << current; //��� cout << index;
  return 0;
}