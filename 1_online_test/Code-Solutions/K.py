n = int(input())
d = list(map(int, input().split()))
dist =[0 for j in range(n)]
res = 0
Kmod = 1000000007

for i in range(n):
    dist[i] = d[i] + d[i]*(d[i]-1)
    for j in range(i):
        dist[i] = dist[i] + (d[j]*d[i])*(i-j+2) + d[j]*(i-j+1) + d[i]*(i-j+1) + i-j
    res = res + dist[i]
    if res > Kmod:
        res = res - Kmod

# print (dist)
print (res)
