n = int(input())
d = list(map(int, input().split()))
dist =[0 for j in range(n)]
nodes =[0 for j in range(n)]
res = 0
Kmod = 1000000007

for i in range(1, n):
    nodes[i] = nodes[i-1] + 1
    dist[i] = dist[i-1] + nodes[i]

    res = res + (dist[i] + d[i]) + (d[i]*(d[i]-1)) + (d[i] * (dist[i] + nodes[i]))
    if res > Kmod:
        res = res%Kmod

    nodes[i] = nodes[i] + d[i]
    dist[i] = dist[i] + d[i]

#print (dist)
if res > Kmod:
    res = res%Kmod
print (res)
