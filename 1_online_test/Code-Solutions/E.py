import numpy

n = int(input())
res = 0

for i in range (1, n):
    res = res + i / (n*n)

print (round(res, 4))
