import numpy

n = int(input())

a =[[0 for j in range(n)] for i in range(n)]

for i in range (n):
    for j in range (n):
        if (i == j):
            a[i][j] = 2
        elif (abs(i-j) == 1):
            a[i][j] = 1
    print (a[i])

ans = numpy.linalg.det(a)
print (ans)
